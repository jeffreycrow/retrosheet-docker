#!/bin/bash

psql -h retrosheet-docker_db_1 -U postgres -c 'CREATE DATABASE retrosheet'
psql -h retrosheet-docker_db_1 -U postgres -d retrosheet -f /opt/py-retrosheet/sql/schema.postgres.sql

