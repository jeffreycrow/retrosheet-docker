#!/bin/bash

cd /var/data/event; for d in $(ls); do cd $d; for f in *; do mv $f "${d}_${f}"; done; cd ..; done; cd ..

find /var/data -mindepth 2 -not -path '*/\.*' -type f -exec mv -t /var/data -i '{}' +
