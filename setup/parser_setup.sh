#!/bin/bash

export DATABASE_ENGINE="postgresql"
export DATABASE_HOST="retrosheet-docker_db_1"
export DATABASE_DATABASE="retrosheet"
export DATABASE_SCHEMA="retrosheet"
export DATABASE_USER="postgres"
export DATABASE_PASSWORD=""
export DOWNLOAD_DIRECTORY=/var/data/event/regular

# set library path for libchadwick
export LD_LIBRARY_PATH=/usr/local/lib

wget -q -P /tmp https://github.com/chadwickbureau/chadwick/releases/download/v0.7.2/chadwick-0.7.2.tar.gz && tar -xf /tmp/chadwick-0.7.2.tar.gz -C /opt

cd /opt/chadwick-0.7.2 && ./configure --quiet && make && make install
