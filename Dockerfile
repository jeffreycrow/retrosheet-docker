# start from base
FROM ubuntu:latest
MAINTAINER Jeff Crow <jeff@jeffreycrow.com

# install system-wide deps for python
RUN apt-get -yqq update && apt-get -yqq install \
python-pip \
python-dev \
postgresql-client \
libpq-dev \
git \
wget \
rename \
nano \
bash-completion \
&& rm -rf /var/lib/apt/lists/*

# install python dependencies
RUN pip install sqlalchemy psycopg2

# copy our application code
ADD py-retrosheet /opt/py-retrosheet

# copy setup files
ADD setup /opt/setup

RUN /opt/setup/parser_setup.sh
